# LayoutAndGeometry

LayoutAndGeometry is a technique project to explore how SwiftUI handles layout.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/layout-and-geometry-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Rules of layout
- Alignment
- Custom guides
- Frames
- Coordinate spaces
- GeometryReader