//
//  LayoutAndGeometryApp.swift
//  LayoutAndGeometry
//
//  Created by Pascal Hintze on 20.03.2024.
//

import SwiftUI

@main
struct LayoutAndGeometryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
